import { Component,HostListener,OnInit  } from '@angular/core';
import { Router, ActivatedRoute,Params } from '@angular/router';
import { AppService } from '../services/app.service';
import { ImageValuesObject } from "../interfaces/imageValue.interface";
import * as SpatialNavigation from '../SpatialNavigation.js'

@Component({
  selector: 'show_image',
  templateUrl: './showImage.component.html',
  styleUrls: ['./app.component.css'],

})
export class showImageComponent {

  private value: ImageValuesObject = new ImageValuesObject();
  imageshow :boolean;
  imageshow2:boolean;
  imageshow3:boolean;
  imageshow4:boolean;
  constructor(private appservice: AppService,private router: Router, private route: ActivatedRoute) {
      // this.appservice.startSpatial();
      this.value = this.appservice.getImageValuesObject();
      console.log("value",this.value);
  }
  ngOnInit(){
  if(this.value == 'img1'){
this.imageshow = true;
this.imageshow2 = false;
this.imageshow3 = false;
this.imageshow4 = false;
  }else if(this.value == 'img2'){
    this.imageshow = false;
this.imageshow2 = true;
this.imageshow3 = false;
this.imageshow4 = false;
  }
  else if(this.value == 'img3'){
    this.imageshow = false;
    this.imageshow2 = false;
    this.imageshow3 = true;
    this.imageshow4 = false;
  }
  else if(this.value == 'img4'){
    this.imageshow = false;
this.imageshow2 = false;
this.imageshow3 = false;
this.imageshow4 = true;
  }
  
  }

  onKeydown(e: any) {
    console.log("hello",e);
    if (e.which === 8) {
    this.router.navigate(['../home'], { relativeTo: this.route });
    SpatialNavigation.clear();
    }
  }
 
}






